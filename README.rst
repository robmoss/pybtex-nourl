Modified citations styles for Pybtex
====================================

This package modifies Pybtex citation styles to suppress URL links for entries
that have DOIs.
It is primarily intended for use with the `Sphinx <http://sphinx-doc.org/>`__
documentation generator (see below).

Usage
-----

Add the following lines to the Sphinx configuration file ``conf.py``:

.. code:: python

    import nourl
    nourl.register()

This will make the following citation styles available: ``alpha-nourl``,
``plain-nourl``, ``unsrt-nourl``, ``unsrtalpha-nourl``.

You can then use these styles with any bibliography directive:

::

   .. bibliography:: bibliography.bib
      :style: unsrt-nourl

License
-------

The code is distributed under the BSD 3-Clause license (see ``LICENSE``).

Installation
------------

Clone this repository and execute:

.. code:: sh

    python setup.py install

If you don't have admin rights, install the package locally:

.. code:: sh

    python setup.py install --user

Dependencies
------------

This package requires `Pybtex <http://pybtex.org/>`__ >= 0.18.
