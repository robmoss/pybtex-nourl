"""
This module modifies Pybtex formatting styles to suppress URL links for
entries that have DOIs.
"""

__author__ = 'Rob Moss <robm.dev@gmail.com>'
__copyright__ = 'Copyright (c) 2015 Rob Moss'
__license__ = 'BSD 3-Clause License'
__version__ = '1.0.0'


def modify_format(fmt_class):
    """
    Modify a formatting style to suppress URL links for entries with DOIs.
    """
    from pybtex.style.template import field, FieldIsMissing

    class Modified(fmt_class):
        def format_url(self, e):
            try:
                field('doi')
                return ''
            except FieldIsMissing:
                super(Modified, self).format_url(e)

    return Modified


def modify_and_register(fmt_class, name=None):
    """
    Modify a formatting style to suppress URL links for entries with DOIs,
    register the modified style with Pybtex, and return the name under which
    the modified style was registered.
    """
    from pybtex.plugin import register_plugin

    fmt = modify_format(fmt_class)
    if name is None:
        fmt_mod = fmt_class.__module__.split('.')[-1]
        name = "{}-nourl".format(fmt_mod)
    register_plugin('pybtex.style.formatting', name, fmt)
    return name


def register():
    """
    Register modified versions of the built-in Pybtex formatting styles:
    ``alpha-nourl``, ``plain-nourl``, ``unsrt-nourl``, ``unsrtalpha-nourl``.
    """
    from pybtex.style.formatting.alpha import Style as AlphaStyle
    from pybtex.style.formatting.plain import Style as PlainStyle
    from pybtex.style.formatting.unsrt import Style as UnsrtStyle
    from pybtex.style.formatting.unsrtalpha import Style as UnsrtAlphaStyle

    modify_and_register(AlphaStyle)
    modify_and_register(PlainStyle)
    modify_and_register(UnsrtStyle)
    modify_and_register(UnsrtAlphaStyle)
