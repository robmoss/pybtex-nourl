#!/usr/bin/env python

from setuptools import setup


setup(
    name='nourl',
    version='1.0.0',
    url='https://bitbucket.org/robmoss/pybtex-nourl',
    description='Modified formatting styles for pybtex',
    license='BSD 3-Clause License',
    author='Rob Moss',
    author_email='robm.dev@gmail.com',
    py_modules=['nourl'],
    install_requires=[
        'pybtex >= 0.18',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development :: Documentation',
        'Topic :: Text Processing',
    ],
)
